import socket as s

host = s.gethostname()
port = 1234
buffer = 8
backlog = 5

socket_ = s.socket(s.AF_INET, s.SOCK_STREAM)
socket_.setsockopt(s.SOL_SOCKET, s.SO_REUSEADDR, 1)
socket_.bind((host, port))
socket_.listen(backlog)


client_socket, addr = socket_.accept()
with client_socket:
    while True:
        #print('Waiting...')
        try:
            data = client_socket.recv(buffer)
            message = f'Message , host: {host}' # + '\r\n'
            client_socket.send(message.encode('utf-8'))
            if data:
                print(f'Connection: {addr}')
                print(data.decode("utf-8"))
            else:
                print("that's it.")
        except:
            break
            #continue
