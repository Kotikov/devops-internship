from bottle import run, post, get, delete, route
from bottle import request, response, static_file
import os
from shutil import rmtree
from json import dumps


path = '/storage'
methods = [{'/': 'all the files we store'},
           {'/<name>': 'downloads file <name> for you'},
           {'/delete': 'deletes the folder'},
           {'/delete/': 'deletes only one file>'},
           {'/post': 'uploads a pic'},
           {'exit': "if you're lost"}]

# насколько нормальна практика


@get('/')
def getall():
    try:
        fileset = []  # чтобы не удалять/добавлять но не знайу нужно подумать
        if os.path.exists(path):
            files = os.listdir(path)
            if len(files) >= 1:
                for file in files:
                    new_file = {'name': file}
                    fileset.append(new_file)
            else:
                resp = {'answer': 'no files detected'}
                fileset.append(resp)
        else:
            os.makedirs(path)
            #response.body = 'The folder has just been created and there are no files in it :)'
        return {'files': fileset}
            #raise ValueError  # response.status == 200
    except ValueError:
        return response.status_code == 400  # вопрос почему 500 выдает впринципе понятно но не очень


@get('/<name>')  # download if exists
def getpic(name):
    pass


@delete('/delete')
@delete('/delete/<name>')  # статус коды нормально как сделать а аАа?
def delpic(name=None):
    if name is None:
        if os.path.exists(path):
            rmtree(path)
            return response.status == 200
        else:
            return response.status == 400
    else:
        if os.path.exists(path):
            if name in os.listdir(path):
                os.remove(os.path.join(path, name))
                return dumps('File was deleted')
                #return response.status == 200
            else:
                return dumps(f'No such file {name}')
                #return response.status == 400
        else:
            os.makedirs(path)
            print('The folder has just been created and there are no files in it :)')
            response.body = {'The folder has just been created and there are no files in it :)'}
            return response


#@get('/post') #удолить кхъ(
#def form():
#    return static_file('index.html', root='.')

@post('/post')
def postpic():
    try:
        upload = request.files.file
        name, ext = os.path.splitext(upload.filename)
        if ext:
            if ext.lower() not in ('.png', '.jpg', '.jpeg'):
                return "File extension is not allowed."
        else:
            return response.status == 400

        if not os.path.exists(path):
            os.makedirs(path)
        file_path = f"{path}/{upload.filename}"
        if not os.path.exists(file_path):
            upload.save(file_path)
            result = f"File {upload.filename} successfully saved to '{path}'."
            response.status = 200
        else:
            result = f"File with such a name: '{upload.filename}' already exists in {path}"
            response.status = 400
        print(result)

    except Exception as e:
        print(e)
        return response.status == 400


if __name__ == '__main__':
    run(port=9129, reloader=True, debug=True)
    #storage = sys.argv[1]
    #path1 = sys.argv[1]
    #getall(path1)

