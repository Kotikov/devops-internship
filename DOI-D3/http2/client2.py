import http.client as client
from server2 import methods
import os


def conn(url):
    flag = 0
    while True:
        response = 'Smth'
        connection = client.HTTPConnection(url)
        if flag == 0:
            for _ in methods:
                print(_)
            endp = input('choose one option: ').replace(' ', '')
            if endp == '/':   # dictionary(?)
                response = getall(connection, endp)
                print(response)
                continue
            elif endp == '/<name>':
                response = getpic(connection, endp)
                print(response)
                continue
            elif endp == '/delete':
                response = delfol(connection, endp)
                print(response)
                continue
            elif endp == '/delete/':
                response = delpic(connection, endp)
                print(response)
                continue
            elif endp == '/post':
                response = postpic(connection, endp)
                print(response)
                continue
            elif endp == 'exit':
                flag = 1
                continue
                #response = postpic(connection)
            else:
                print('Undefined method')
                continue
            #connection.close()
        else:
            connection.close()
        #return response


def getall(connection, endp):
    request = connection.request('GET', endp)
    response = connection.getresponse()
    return f'endpoint {endp} \n response status {response.status} \n  response: {response.read()}'


def getpic(connection, endp):
    pass


def delpic(connection, endp):
    name = input('Enter pic name')
    request = connection.request('DELETE', endp + name)
    response = connection.getresponse()
    return f'endpoint {endp} \n response status {response.status} \n  response: {response.read()}'


def delfol(connection, endp):
    print('are you sure you want to delete the directory?')
    resp = input('y|n')
    if resp.replace(' ', '').lower() == 'y':
        #pass
        #return response.status == 200
        request = connection.request('DELETE', endp)
        response = connection.getresponse()
        return f'endpoint {endp} \n response status {response.status} \n  response: {response.read()}'
    elif resp.replace(' ', '').lower() == 'n':
        return 'Oops'
    else:
        return 'Incorrect answer was sent'


def postpic(connection, endp):
    file = os.path.abspath(input('write the filename'))
    request = connection.request('POST', endp, body=open(file, 'rb'))#, headers={'Transfer-Encoding': 'chunked'})
    response = connection.getresponse()
    return f'endpoint {endp} \n response status {response.status} \n  response: {response.read()}'


if __name__ == '__main__':
    url = '127.0.0.1:9129'
    conn(url)


