import requests as r
import tldextract as t
import os
import zlib

url = 'https://www.instagram.com/?hl=ru'
urll = 'https://mrk-bsuir.by/'
folder = 'DOI_unzip'
path = os.getcwd()


def main(url, path, folder):
    domain = t.extract(url).domain
    if os.path.isdir(folder):
        pass
    else:
        os.mkdir(folder)
    filename = path + "/" + folder + "/" + domain + ".txt"
    #try:
    file = unzip(url, filename)
    return file
    #except:
     #   print('Something went wrong')


def unzip(url, filename):
    gzip_header = {'Accept-Encoding': 'gzip'}
    response = r.get(url, headers=gzip_header, stream=True)  # verify false(?) verify=False,  --ssl
    rawdata = response.raw.stream()
    decompr = decompress(rawdata)
    with open(filename, 'w+') as file:
        for chunk in decompr:
            chunk = str(chunk)
            file.write(chunk + '\n')
            file.flush()
    return file


def decompress(rawdata):
    decompr = zlib.decompressobj(16 + zlib.MAX_WBITS)
    for chunk in rawdata:
        yield decompr.decompress(chunk)


main(urll, path, folder)
